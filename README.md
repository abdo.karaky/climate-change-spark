# Climate Change Spark

This is my implementation for the [Function Programming in Scala Specialization](https://www.coursera.org/specializations/scala) [capstone project](https://www.coursera.org/learn/scala-capstone).

This application shows interactive visualizations of the evolution of temperatures over time all over the world.  

The map tiles are generated using Scala and Spark by manipulating a large temperature dataset (several GBs) collected between 1975 and 2015.

The development of this application involved: 
- Transforming data provided by weather stations into meaningful information like, for instance, the average temperature of each point of the globe over the last ten years. This is done using Spark.

- Making images from this information by using spatial and linear interpolation techniques.

- Implementing a responsive user interface using functional reactive programming (FRP).
