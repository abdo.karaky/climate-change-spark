import java.nio.file.{Files, Paths}

import com.sksamuel.scrimage.Image
import observatory.Manipulation.{average, deviation}
import observatory.Visualization.visualize
import observatory.Extraction.{locateTemperatures, locationYearlyAverageRecords}
import observatory.Interaction.tile
import observatory.Visualization2.visualizeGrid
import observatory.{Color, Temperature, Tile, Year}

object Main extends App {

  def saveTile(tileImg: Image, year: Year, x: Int, y: Int, z: Int) = {
    val path = Paths.get(s"target/temperatures/$year/$z/$x-$y.png")
    Files.createDirectories(path.getParent)
    tileImg.output(path)
  }

  println(">>> Location temperatures")
  val temp1975 = locateTemperatures(1975, "/stations.csv", "/1975.csv")
  val temp1990 = locateTemperatures(1990, "/stations.csv", "/1990.csv")
  val temp2015 = locateTemperatures(2015, "/stations.csv", "/2015.csv")
  println(">>> Location yearly average records")
  val avg1975 = locationYearlyAverageRecords(temp1975)
  val avg1990 = locationYearlyAverageRecords(temp1990)
  val avg2015 = locationYearlyAverageRecords(temp2015)

  val tempColors: Iterable[(Temperature, Color)] = List(
    (60, Color(255, 255, 255)),
    (32, Color(255, 0, 0)),
    (12, Color(255, 255, 0)),
    (0, Color(0, 255, 255)),
    (-15, Color(0, 0, 255)),
    (-27, Color(255, 0, 255)),
    (-50, Color(33, 0, 107)),
    (-60, Color(0, 0, 0))
  )

  val devColors: Iterable[(Temperature, Color)] = List(
    (7, Color(0, 0, 0)),
    (4, Color(255, 0, 0)),
    (2, Color(255, 255, 0)),
    (0, Color(255, 255, 255)),
    (-2, Color(0, 255, 255)),
    (-7, Color(0, 0, 255))
  )

  println(">>> Visualize 1975")
  val img1975 = visualize(avg1975, tempColors)
  img1975.output(new java.io.File("target/img1975.png"))
  println(">>> Visualize 1990")
  val img1990 = visualize(avg1990, tempColors)
  img1990.output(new java.io.File("target/img1990.png"))
  println(">>> Visualize 2015")
  val img2015 = visualize(avg2015, tempColors)
  img2015.output(new java.io.File("target/img2015.png"))


  println(">>> Interaction 1975")
  val tile1975 = tile(avg1975, tempColors, Tile(0, 0, 0))
  saveTile(tile1975, 1975, 0, 0, 0)
  println(">>> Interaction 1990")
  val tile1990 = tile(avg1990, tempColors, Tile(0, 0, 0))
  saveTile(tile1990, 1990, 0, 0, 0)
  println(">>> Interaction 2015")
  val tile2015 = tile(avg2015, tempColors, Tile(0, 0, 0))
  saveTile(tile2015, 2015, 0, 0, 0)


  println(">>> Average 1975 - 1990")
  val normals = average(List(avg1975, avg1990))

  println(">>> Deviation 2015 with 1975-1990")
  val dev2015 = deviation(avg2015, normals)

  println(">>> Visualize grid 2015")
  val imgDev2015 = visualizeGrid(dev2015, devColors, Tile(0, 0, 0))
  imgDev2015.output(new java.io.File("target/imgDev2015.png"))
}
