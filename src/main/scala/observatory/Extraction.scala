package observatory

import java.time.LocalDate

import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source


/**
  * 1st milestone: data extraction
  */
object Extraction extends ExtractionInterface {
  val logger: Logger = Logger.getLogger("Extraction")
  Logger.getLogger("org.apache.spark").setLevel(Level.WARN)

  @transient lazy val conf: SparkConf = new SparkConf().setMaster("local[*]").setAppName("Extraction")
  @transient lazy val sc: SparkContext = new SparkContext(conf)

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Year, stationsFile: String,
                         temperaturesFile: String): Iterable[(LocalDate, Location, Temperature)] = {
    val stationsLines =
      Source.fromInputStream(getClass.getResourceAsStream(stationsFile), "utf-8").getLines().toSeq
    val temperaturesLines =
      Source.fromInputStream(getClass.getResourceAsStream(temperaturesFile), "utf-8").getLines().toSeq
    sparkLocalTemperatures(year, sc.parallelize(stationsLines), sc.parallelize(temperaturesLines)).collect().toSeq
  }

  private def sparkLocalTemperatures(year: Year, stationsLines: RDD[String],
                             temperaturesLines: RDD[String]): RDD[(LocalDate, Location, Temperature)] = {
    val stationsData = convertStationsLines(stationsLines).filter(_._2.isDefined).mapValues(x => x.get)
    val temperaturesData = convertTemperaturesLines(year, temperaturesLines)
    stationsData.join(temperaturesData).map { case (_, (loc, (date, temp))) => (date, loc, temp) }
  }

  def convertStationsLines(lines: RDD[String]): RDD[((Option[Int], Option[Int]), Option[Location])] = {
    def createLocation(lat: String, lon: String): Option[Location] =
      if (lat == "" || lon == "") None else Some(Location(lat.toDouble, lon.toDouble))

    lines
      .map(line => {
        val arr = line.split(",", -1)
        val stnId = if (arr(0) == "") None else Some(arr(0).toInt)
        val wbanId = if (arr(1) == "") None else Some(arr(1).toInt)
        val location = createLocation(arr(2), arr(3))
        ((stnId, wbanId), location)
      })
  }


  def convertTemperaturesLines(year: Year,
                               lines: RDD[String]): RDD[((Option[Int], Option[Int]), (LocalDate, Temperature))] = {
    def convertFahrenheitToCelsius(degree: Double): Double = (degree - 32) * 5 / 9

    lines.map(line => {
      val arr = line.split(",", -1)
      val stnId = if (arr(0) == "") None else Some(arr(0).toInt)
      val wbanId = if (arr(1) == "") None else Some(arr(1).toInt)
      val localDate = LocalDate.of(year, arr(2).toInt, arr(3).toInt)
      val temperature = convertFahrenheitToCelsius(arr(4).toDouble)
      ((stnId, wbanId), (localDate, temperature))
    })
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Temperature)]): Iterable[(Location, Temperature)] = {
    sparkLocationYearlyAverageRecords(sc.parallelize(records.toSeq)).collect().toSeq
  }

  private def sparkLocationYearlyAverageRecords(records: RDD[(LocalDate, Location, Temperature)]): RDD[(Location, Temperature)] = {
    val recordsByYear = records.map {case (date, loc, temp) => ((date.getYear, loc), (temp, 1))}
    recordsByYear
      .reduceByKey((r1, r2) => (r1._1 + r2._1, r1._2 + r2._2))
      .map {case ((_, loc), (temp, count)) => (loc, temp / count)}
  }

}
