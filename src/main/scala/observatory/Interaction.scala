package observatory

import com.sksamuel.scrimage.{Image, Pixel}
import observatory.Visualization.{interpolateColor, predictTemperature}
import org.apache.log4j.Logger


/**
  * 3rd milestone: interactive visualization
  */
object Interaction extends InteractionInterface {
  val logger: Logger = Logger.getLogger("Interaction")

  /**
    * @param tile Tile coordinates
    * @return The latitude and longitude of the top-left corner of the tile,
    *         as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(tile: Tile): Location = {
    val n = 1 << tile.zoom
    val lonDeg = tile.x.toDouble / n * 360.0 - 180.0
    val latRad = math.atan(math.sinh(math.Pi * (1 - 2 * tile.y.toDouble / n)))
    Location(latRad.toDegrees, lonDeg)
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @param tile Tile coordinates
    * @return A 256×256 image showing the contents of the given tile
    */
  def tile(temperatures: Iterable[(Location, Temperature)],
           colors: Iterable[(Temperature, Color)], tile: Tile): Image = {
    val pos = for {
      y <- 0 until 256
      x <- 0 until 256
    } yield (x, y)

    val z = 1 << 8
    val (subX, subY) = (tile.x * z, tile.y * z)

    val pixels = pos.par.map {
      case (x, y) =>
        val subTile = Tile(subX + x, subY + y, tile.zoom + 8)
        val temp = predictTemperature(temperatures, tileLocation(subTile))
        val color = interpolateColor(colors, temp)
        Pixel(color.red, color.green, color.blue, 127)
    }

    Image(256, 256, pixels.toArray)
  }

  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    * @param yearlyData Sequence of (year, data), where `data` is some data associated with
    *                   `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](yearlyData: Iterable[(Year, Data)],
                          generateImage: (Year, Tile, Data) => Unit): Unit = {
    for {(year, data) <- yearlyData
         zoom <- 0 to 3} {
      val n = 1 << zoom
      for (x <- 0 until n; y <- 0 until n)
        generateImage(year, Tile(x, y, zoom), data)
    }
  }
}
