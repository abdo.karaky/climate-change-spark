package observatory

import com.sksamuel.scrimage.{Image, Pixel}
import org.apache.log4j.Logger

/**
  * 2nd milestone: basic visualization
  */
object Visualization extends VisualizationInterface {
  val logger: Logger = Logger.getLogger("Visualization")


  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Temperature)], location: Location): Temperature = {
    def inverseDistanceWeighting(temperatures: Iterable[(Location, Temperature)], location: Location, power: Int)
                                (distanceFunc: (Location, Location) => Double, threshold: Int): Temperature = {
      var tempSum, weightSum = 0.0
      for ((loc, temp) <- temperatures) {
        val dist = distanceFunc(loc, location)
        if (dist < threshold) return temp
        val weight = 1 / math.pow(dist, power)
        weightSum += weight
        tempSum += temp * weight
      }
      tempSum / weightSum
    }

    def greatCircleDistance(loc1: Location, loc2: Location): Double = {
      def isAntipodes = (loc1.lat + loc2.lat == 0) && (math.abs(loc1.lon - loc2.lon) == 180)
      def generalFormula: Double = {
        val (lat1, lon1, lat2, lon2) =
          (loc1.lat.toRadians, loc1.lon.toRadians, loc2.lat.toRadians, loc2.lon.toRadians)
        val sinTerm = math.sin(lat1) * math.sin(lat2)
        val cosTerm = math.cos(lat1) * math.cos(lat2) * math.cos(math.abs(lon1 - lon2))
        math.acos(sinTerm + cosTerm)
      }
      val deltaSigma = if (loc1 == loc2) 0.0 else if (isAntipodes) math.Pi else generalFormula
      val earthRadius = 6.371e6
      earthRadius * deltaSigma
    }

    inverseDistanceWeighting(temperatures, location, 6)(greatCircleDistance, 1000)
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Temperature, Color)], value: Temperature): Color = {
    def linearInterpolator(lowerBound: (Temperature, Color), higherBound: (Temperature, Color)): Color = {
      val weight = (value - lowerBound._1) / (higherBound._1 - lowerBound._1)
      val (lowerR, lowerG, lowerB) = (lowerBound._2.red, lowerBound._2.green, lowerBound._2.blue)
      val (higherR, higherG, higherB) = (higherBound._2.red, higherBound._2.green, higherBound._2.blue)

      Color((lowerR * (1 - weight) + higherR * weight).round.intValue(),
        (lowerG * (1 - weight) + higherG * weight).round.intValue(),
        (lowerB * (1 - weight) + higherB * weight).round.intValue()
      )
    }

    def findBounds: (Option[(Temperature, Color)], Option[(Temperature, Color)]) = {
      var lowerBound, higherBound: Option[(Temperature, Color)] = None
      for (p <- points) {
        val (temp, _) = p
        if (temp == value) return (Some(p), Some(p))
        if (temp < value && (lowerBound.isEmpty || temp > lowerBound.get._1)) lowerBound = Some(p)
        if (temp > value && (higherBound.isEmpty || temp < higherBound.get._1)) higherBound = Some(p)
      }
      (lowerBound, higherBound)
    }

    findBounds match {
      case (Some(l), Some(h)) => if (l == h) l._2 else linearInterpolator(l, h)
      case (Some(l), None) => l._2
      case (None, Some(h)) => h._2
    }
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)]): Image = {
    val locations = for {
      y <- 0 until 180
      x <- 0 until 360
    } yield Location(90 - y, x - 180)

    val pixels = locations.par.map(loc => {
      val temp = predictTemperature(temperatures, loc)
      val color = interpolateColor(colors, temp)
      Pixel(color.red, color.green, color.blue, 127)
    })

    Image(360, 180, pixels.toArray)
  }
}