package observatory

import com.sksamuel.scrimage.{Image, Pixel}
import observatory.Interaction.tileLocation
import observatory.Visualization.interpolateColor

/**
  * 5th milestone: value-added information visualization
  */
object Visualization2 extends Visualization2Interface {

  /**
    * @param point (x, y) coordinates of a point in the grid cell
    * @param d00 Top-left value
    * @param d01 Bottom-left value
    * @param d10 Top-right value
    * @param d11 Bottom-right value
    * @return A guess of the value at (x, y) based on the four known values, using bilinear interpolation
    *         See https://en.wikipedia.org/wiki/Bilinear_interpolation#Unit_Square
    */
  def bilinearInterpolation(
    point: CellPoint,
    d00: Temperature,
    d01: Temperature,
    d10: Temperature,
    d11: Temperature
  ): Temperature = {
    val v00 = d00 * (1 - point.x) * (1 - point.y)
    val v01 =  d01 * (1 - point.x) * point.y
    val v10 = d10 * point.x * (1 - point.y)
    val v11 = d11 * point.x * point.y
    v00 + v01 + v10 + v11
  }

  /**
    * @param grid Grid to visualize
    * @param colors Color scale to use
    * @param tile Tile coordinates to visualize
    * @return The image of the tile at (x, y, zoom) showing the grid using the given color scale
    */
  def visualizeGrid(
    grid: GridLocation => Temperature,
    colors: Iterable[(Temperature, Color)],
    tile: Tile
  ): Image = {

    val pos = for {
      y <- 0 until 256
      x <- 0 until 256
    } yield (x, y)

    val z = 1 << 8
    val (subX, subY) = (tile.x * z, tile.y * z)

    val pixels = pos.par.map {
      case (x, y) =>
        val subTileLoc = tileLocation(Tile(subX + x, subY + y, tile.zoom + 8))
        val (lat00, lon00) = (math.floor(subTileLoc.lat).toInt, math.floor(subTileLoc.lon).toInt)
        val d00 = grid(GridLocation(lat00, lon00))
        val d01 = grid(GridLocation(lat00 + 1, lon00))
        val d10 = grid(GridLocation(lat00, lon00 + 1))
        val d11 = grid(GridLocation(lat00 + 1, lon00 + 1))
        val temp = bilinearInterpolation(CellPoint(subTileLoc.lon - lon00, subTileLoc.lat - lat00),
          d00, d01, d10, d11)
        val color = interpolateColor(colors, temp)
        Pixel(color.red, color.green, color.blue, 127)
    }

    Image(256, 256, pixels.toArray)
  }
}
